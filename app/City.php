<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    protected $appends = array('full_name');

    //
    public function getFullNameAttribute() {
        return $this->name . ', ' . $this->name_kz . ', ' . $this->region;
    }
}
