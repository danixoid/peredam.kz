<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Carrier extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name', 'phone', 'transport_id', 'departure_at', 'user_id',
        'city_from_id', 'city_to_id', 'arrival_at', 'price_start', 'price_end', 'notes',
    ];

    protected $casts = [
        'departure_at' => 'datetime',
        'arrival_at' => 'datetime',
    ];

    public function user() {
        return $this->belongsTo('App\User');
    }
    
    public function transport() {
        return $this->belongsTo('App\Transport');
    }

    public function city_from() {
        return $this->belongsTo('App\City','city_from_id');
    }

    public function city_to() {
        return $this->belongsTo('App\City','city_to_id');
    }

    public function hasAttribute($attr) {
        return array_key_exists($attr, $this->attributes);
    }
}
