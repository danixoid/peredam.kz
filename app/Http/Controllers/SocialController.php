<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Socialite;
use App\User;

class SocialController extends Controller
{
        
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {
        try {
            $user = Socialite::driver($provider)->user();

            $finduser = User::where('provider_id', $user->id)->first();
            
            if ($finduser) {
                \Auth::login($finduser);
                return redirect('/');
            } else {
                $password = \Hash::make('password');
                $newUser = User::create([
                    'name'          => $user->name,
                    'email'         => $user->email,
                    'password'      => $password,
                    'provider'      => $provider,
                    'provider_id'   => $user->id
                ]);
                auth()->login($newUser);

                $details = [
                    'title' => 'Ваш новый пароль на PEREDAM.KZ',
                    'body' => 'Ваш пароль <br /> <pre>' . $password . '<pre>'
                ];
    
                \Mail::to($newUser->email)->send(new \App\Mail\SendPassword($details));
                
                return redirect()->back();
            }
        } catch(Exception $e) {
            return redirect('auth');
        }
    }
}
