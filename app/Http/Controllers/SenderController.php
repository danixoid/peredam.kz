<?php

namespace App\Http\Controllers;

use App\Sender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class SenderController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index','create','store','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = request()->all();

        $senders = \App\Sender::whereNotNull('id');
        foreach($data as $key=>$value) {
            if(isset($value) && Schema::hasColumn((new \App\Sender())->getTable(), $key)) {
                $senders = $senders->where($key,$value);
            }
        }
        
        return view('sender.index',['senders' => $senders->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sender.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());
        $data = $request->all();
        
        if(isset($data['dates'])) {
            $dates = explode(' - ',$data['dates']);
            unset($data['dates']);
            $data['date_from'] = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/','$3-$2-$1',$dates[0]);
            $data['date_to'] = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/','$3-$2-$1',$dates[1]);
        }

        if(\Auth::check()) {
            $data['user_id'] = \Auth::user()->id;
        }

        $sender = \App\Sender::create($data);

        if(isset($sender))
            return redirect()->route('sender.index');


        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sender  $sender
     * @return \Illuminate\Http\Response
     */
    public function show(Sender $sender)
    {
        if($sender->user_id > 0) $this->middleware('auth');
        return view('sender.show',['sender' => $sender]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sender  $sender
     * @return \Illuminate\Http\Response
     */
    public function edit(Sender $sender)
    {
        if($sender->user_id == \Auth::user()->id) return view('sender.edit',['sender' => $sender]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sender  $sender
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sender $sender)
    {

        $data = $request->all();
        if(isset($data['dates'])) {
            $dates = explode(' - ',$data['dates']);
            unset($data['dates']);
            $data['date_from'] = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/','$3-$2-$1',$dates[0]);
            $data['date_to'] = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/','$3-$2-$1',$dates[1]);
        }

        $sender->update($data);
        $sender->save();

        return redirect()->route('sender.show',$sender);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sender  $sender
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sender $sender)
    {
        $sender->delete();
        return redirect()->route('sender.index')->withInput();
    }
}
