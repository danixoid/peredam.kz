<?php

namespace App\Http\Controllers;

use App\Carrier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class CarrierController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = request()->all();
        $departure_at = null;
        $arrival_at = null;

        if(isset($data['dates'])) {
            $dates = explode(' - ',$data['dates']);
            unset($data['dates']);
            $departure_at = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/','$3-$2-$1',$dates[0]);
            $arrival_at = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/','$3-$2-$1',$dates[1]);
        }

        $carriers = \App\Carrier::whereNotNull('id');

        if(isset($departure_at)) {
            $carriers = $carriers->where('departure_at','>=',$departure_at);
        }
        if(isset($arrival_at)) {
            $carriers = $carriers->where('arrival_at','<=',$arrival_at);
        }

        foreach($data as $key=>$value) {
            
            if(isset($data[$key]) && Schema::hasColumn((new \App\Carrier())->getTable(), $key)) {
                $carriers = $carriers->where($key,$value);
            }
        }
        return view('carrier.index',['carriers' => $carriers->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('carrier.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        
        if(isset($data['dates'])) {
            $dates = explode(' - ',$data['dates']);
            unset($data['dates']);
            $data['departure_at'] = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/','$3-$2-$1',$dates[0]);
            $data['arrival_at'] = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/','$3-$2-$1',$dates[1]);
        }
        // dd($data);

        if(\Auth::check()) {
            $data['user_id'] = \Auth::user()->id;
        }

        $sender = \App\Carrier::create($data);

        if(isset($sender))
            return redirect()->route('carrier.index');


        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function show(Carrier $carrier)
    {
        return view('carrier.show',['carrier' => $carrier]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function edit(Carrier $carrier)
    {
        if($carrier->user_id == \Auth::user()->id) return view('carrier.edit',['carrier' => $carrier]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Carrier $carrier)
    {
        $data = $request->all();
        if(isset($data['dates'])) {
            $dates = explode(' - ',$data['dates']);
            unset($data['dates']);
            $data['departure_at'] = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/','$3-$2-$1',$dates[0]);
            $data['arrival_at'] = preg_replace('/^(\d{2}).(\d{2}).(\d{4})$/','$3-$2-$1',$dates[1]);
        }

        $carrier->update($data);
        $carrier->save();

        return redirect()->route('carrier.show',$carrier);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Carrier  $carrier
     * @return \Illuminate\Http\Response
     */
    public function destroy(Carrier $carrier)
    {
        $carrier->delete();
        return redirect()->route('carrier.index')->withInput();
    }
}
