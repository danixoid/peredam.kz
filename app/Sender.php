<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sender extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name_from', 'phone_from', 'city_from_id', 'address_from', 'user_id',
        'name_to', 'phone_to', 'city_to_id', 'address_to', 'price', 'notes',
        'date_from', 'date_to'
    ];

    protected $casts = [
        'date_from' => 'datetime',
        'date_to' => 'datetime',
    ];
    //

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function city_from() {
        return $this->belongsTo('App\City','city_from_id');
    }

    public function city_to() {
        return $this->belongsTo('App\City','city_to_id');
    }

    public function hasAttribute($attr) {
        return array_key_exists($attr, $this->attributes);
    }

}
