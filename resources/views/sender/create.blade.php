

@extends('app')

@section('content')


	<form class="contact100-form validate-form" action="{!! route('sender.store') !!}" method="POST">
		@csrf
		<span class="contact100-form-title">
			Нужно передать
		</span>
		<span class="contact100-form-desc">
			Создать объявление
		</span>
		
		<div class="wrap-input100 validate-input bg0 rs1-alert-validate" data-validate="Заполните описание">
			<i class="fa fa-envelope-o"></i>
			<span class="label-input100">Описание передачи</span>
			<textarea class="input100" name="notes" placeholder="например, коробка с вещами весом 4кг, габариты 50см на 50см на 40см"></textarea>
		</div>

		{{--  --}}

		<div class="wrap-input100 validate-input bg1" data-validate="Обязательное поле">
			<i class="fa fa-calendar"></i>
			<span class="label-input100">Даты отправления - доставки</span>
			<input class="input100" type="text" name="dates">
		</div>

		<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate="Обязательное поле">
			<i class="fa fa-user-circle"></i>
			<span class="label-input100">Имя отправителя</span>
			@if(\Auth::check())
			<input type="hidden" name="name_from" value="{!! \Auth::check() ?  \Auth::user()->name : '' !!}" />
			@endif
			<input class="input100" type="text" @if(\Auth::check()) disabled @endif  name="name_from" value="{!! \Auth::check() ?  \Auth::user()->name : '' !!}" placeholder="например, Кажыгумар" required>
		</div>

		<div class="wrap-input100 bg1 rs1-wrap-input100">
			<i class="fa fa-phone"></i>
			<span class="label-input100">Контактный телефон отправителя</span>
			<input class="input100" type="text" name="phone_from" value="" placeholder="+7 (777) 426 05 76">
		</div>

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-up"></i>
			<span class="label-input100">Город отправления</span> 
			<div>
				<select class="js-select2" name="city_from_id" placeholder="{!! __('Выберите город отправления') !!}">
					@foreach (\App\City::all() as $city)
					<option value="{!! $city->id !!}">{!! $city->name . ', ' . $city->name_kz . ', ' . $city->region !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		<div class="wrap-input100 validate-input bg1 rs1-wrap-input100">
			<i class="fa fa-address-card"></i>
			<span class="label-input100">Введите адрес отправления</span>
			<input class="input100" type="text" name="address_from"
				placeholder="например, мкр.Самал-2, 87, кв. 55">
		</div>

		<div class="wrap-input100 bg1 rs1-wrap-input100">
			<i class="fa fa-user-circle"></i>
			<span class="label-input100">Имя получателя</span>
			<input class="input100" type="text" name="name_to"
				placeholder="например, Габдуллин Аман">
		</div>

		<div class="wrap-input100 bg1 rs1-wrap-input100">
			<i class="fa fa-phone"></i>
			<span class="label-input100">Контактный телефон получателя</span>
			<input class="input100" type="text" name="phone_to"
				placeholder="+7 (777) 426 05 76">
		</div>

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-down"></i>
			<span class="label-input100">Город доставки</span>
			<div>
				<select class="js-select2" name="city_to_id" placeholder="{!! __('Выберите город доставки') !!}">
					@foreach (\App\City::all() as $city)
					<option value="{!! $city->id !!}">{!! $city->name . ', ' . $city->name_kz . ', ' . $city->region !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		<div class="wrap-input100 bg1 rs1-wrap-input100">
			<i class="fa fa-address-card"></i>
			<span class="label-input100">{{__('Введите адрес доставки')}}</span>
			<input class="input100" type="text" name="address_to" placeholder="например, ул.Бозтаева, 39, кв. 67">
		</div>

		<div class="rs1-wrap-input100">
			<a class="contact100-form-btn" href="{!! route('sender.index') !!}">
				<span>
					{!! __('Go Back') !!}
					<i class="fa fa-arrow-left m-l-7" aria-hidden="true"></i>
				</span>
			</a>
		</div>

		<div class="rs1-wrap-input100">
			<button class="contact100-form-btn" type="submit">
				<span>
					{!! __('Подать объявление') !!}
					<i class="fa fa-edit m-l-7" aria-hidden="true"></i>
				</span>
			</button>
		</div>
	</form>



@endsection


@section('js')
<!--===============================================================================================-->
<script src="/vendor/daterangepicker/moment.min.js"></script>
<script src="/vendor/daterangepicker/daterangepicker.js"></script>
<script>
	$('input[name="dates"]').daterangepicker({
		timePicker: false,
		locale: {
			format: 'DD/MM/YYYY'
		}
	});
</script>
<!--===============================================================================================-->
	<script src="/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
@endsection