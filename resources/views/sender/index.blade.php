

@extends('app')

@section('content')

	<form class="contact100-form" action="{!! route('sender.index') !!}" method="GET">
		{{-- @csrf --}}
		<span class="contact100-form-title">
			Нужно отправить
		</span>
		<span class="contact100-form-desc">
			Список объявлений на отправление
		</span>

		{{-- <div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-up"></i>
			<span class="label-input100">Способ доставки</span>
			<div>
				<select class="js-select2" id="transport" name="transport_id">
					<option value="">{!! __('Выберите значение') !!}</option>
					@foreach (\App\Transport::all() as $transport)
					<option value="{!! $transport->id !!}"
						@if(request('transport_id') == $transport->id) selected @endif
					>{!! $transport->name !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div> --}}

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-up"></i>
			<span class="label-input100">Город отправления</span> 
			<div>
				<select class="js-select2" name="city_from_id" placeholder="{!! __('Выберите город отправления') !!}">
					<option value="">{!! __('Выберите значение') !!}</option>
					@foreach (\App\City::all() as $city)
					<option
						@if($city->id == request('city_from_id')) selected @endif
						value="{!! $city->id !!}">{!! $city->name . ', ' . $city->name_kz . ', ' . $city->region !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-down"></i>
			<span class="label-input100">Город доставки</span>
			<div>
				<select class="js-select2" name="city_to_id" placeholder="{!! __('Выберите город доставки') !!}">
					<option value="">{!! __('Выберите значение') !!}</option>
					@foreach (\App\City::all() as $city)
					<option 
						@if($city->id == request('city_to_id')) selected @endif
						value="{!! $city->id !!}">{!! $city->name . ', ' . $city->name_kz . ', ' . $city->region !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		<div class="rs1-wrap-input100">
			<button class="contact100-form-btn">
				<span>
					{!! __('Search') !!}
					<i class="fa fa-search m-l-7" aria-hidden="true"></i>
				</span>
			</button>
		</div>
		

	</form>

	<div class="contact100-form mt-2">
		@foreach ($senders as $sender)
		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			{{-- <img class="card-img-top" src="/" alt="Card image cap"> --}}
			<div class="card-body">
				<h5 class="card-title">{!! $sender->name !!}</h5>
				<p class="card-text"><strong><pre>{!! $sender->notes !!}</pre></strong></p>
				<p class="card-text"><strong>{!! __('Город отправления/доставки') !!}</strong>: {!! $sender->city_from->name !!} / {!! $sender->city_to->name !!}</p>
				<p class="card-text"><strong>{!! __('Время отправления/доставки') !!}</strong>: {!! date('d/m/Yг',strtotime($sender->date_from)) !!} / {!! date('d/m/Yг',strtotime($sender->date_to)) !!}</p>
				@if(!$sender->auth_id || \Auth::check())
				<a href="{!! route('sender.show',$sender->id) !!}" class="btn btn-link">{{__('Show')}}</a>
				@else
				<a href="{!! route('login') !!}" class="btn btn-link">{!! __('Чтобы увидеть детали, войдите в систему') !!}</a>
				@endif
			</div>
		</div>	
		@endforeach

	</div>

	<div class="contact100-form mt-2">
		<div class="rs1-wrap-input100 mt-2">
			<a href="{!! route('sender.create') !!}" class="contact100-form-btn">
				<span>
					{!! __('Новое отправление')!!}
					<i class="fa fa-plus m-l-7" aria-hidden="true"></i>
				</span>
			</a>
		</div>
	</div>



@endsection


@section('js')
<!--===============================================================================================-->
	<script src="/vendor/daterangepicker/moment.min.js"></script>
	<script src="/vendor/daterangepicker/daterangepicker.js"></script>
	<script>
		$('input[name="dates"]').daterangepicker({
			timePicker: false,
			@if(request()->has('dates'))
			startDate: moment('{!! explode(' - ',request('dates'))[0] !!}','DD/MM/YYYY'),
			endDate: moment('{!! explode(' - ',request('dates'))[1] !!}','DD/MM/YYYY'),
			@endif
			locale: {
				format: 'DD/MM/YYYY'
			}
		});
	</script>
<!--===============================================================================================-->
	<script src="/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/vendor/noui/nouislider.min.js"></script>
	<script>
	    var filterBar = document.getElementById('filter-bar');

	    noUiSlider.create(filterBar, {
	        start: [ 1500, 3900 ],
	        connect: true,
	        range: {
	            'min': 1500,
	            'max': 7500
	        }
	    });

	    var skipValues = [
	    document.getElementById('value-lower'),
	    document.getElementById('value-upper')
	    ];

	    filterBar.noUiSlider.on('update', function( values, handle ) {
	        skipValues[handle].innerHTML = Math.round(values[handle]);
	        $('.contact100-form-range-value input[name="from-value"]').val($('#value-lower').html());
	        $('.contact100-form-range-value input[name="to-value"]').val($('#value-upper').html());
	    });
	</script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
@endsection