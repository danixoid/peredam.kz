<!DOCTYPE html>
<html lang="en">
<head>
	<title>Peredam.kz</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/vendor/noui/nouislider.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="/css/util.css">
	<link rel="stylesheet" type="text/css" href="/css/main.css">
<!--===============================================================================================-->

@yield("css")
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="/#">PEREDAM.KZ</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
		  	<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
			<div class="navbar-nav">
				<a class="nav-item nav-link {{ Route::currentRouteName() == 'sender.index' ? 'active' : '' }}" href="{{ route('sender.index') }}">Поиск передачи</a>
				@if(\Auth::check())
				<a class="nav-item nav-link {{ Route::currentRouteName() == 'carrier.index' ? 'active' : '' }}" href="{{ route('carrier.index') }}">Поиск пассажира</a>
				@else
				@endif
				{{-- <a class="nav-item nav-link disabled" disabled  href="#">Помощь</a> --}}
			</div>
		</div>
		<div class="collapse navbar-collapse float-right">
			<div class="navbar-nav">
				{{-- @foreach (["ru","en"] as $locale)
					<a class="nav-item nav-link {{ $locale == App::getLocale() ? 'active' : '' }}" href="/lang/change/{{ $locale }}">{{ $locale }}</a>
				@endforeach --}}

				@if(\Auth::check())
				<form id="logout" action="{{ route('logout') }}" method="post">@csrf</form>
				<a class="nav-item nav-link" href="javascript:$('#logout').submit()">[{!! \Auth::user()->name !!}] Выход</a>
				@else
				<a class="nav-item nav-link {{ Route::currentRouteName() == 'login' ? 'active' : '' }}" href="{{ route('login') }}">Войти</a>
				@endif
			</div>
		</div>
    </nav>
    



	<div class="container-contact100 bg-2">
		<div class="wrap-contact100">
			<nav class="nav nav-pills nav-fill mb-5">
				<a class="nav-item nav-link {{ Route::currentRouteName() == 'sender.create' ? 'active' : '' }}" href="{{ route('sender.create') }}"><h3>Нужно передать</h3></a>
				<a class="nav-item nav-link {{ Route::currentRouteName() == 'carrier.create' ? 'active' : '' }}" href="{{ route('carrier.create') }}"><h3>Могу передать</h3></a>
			</nav>
			@yield("content")
		</div>
	</div>


<!--===============================================================================================-->
	<script src="/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="/vendor/bootstrap/js/popper.js"></script>
	<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="/js/main.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="/https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

@yield("js")
</body>
</html>