
@extends('app')
@section('content')

	<form class="contact100-form validate-form" action="{!! route('carrier.store') !!}" method="POST">
		@csrf
		<span class="contact100-form-title">
			{!! __('Могу передать') !!}
		</span>
		<span class="contact100-form-desc">
			{!! __('Регистрация билета') !!}
		</span>

		<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate="Обязательное поле">
			<i class="fa fa-user-circle"></i>
			<span class="label-input100">{!! __('Имя курьера') !!}</span>
			@if(\Auth::check())
			<input type="hidden" name="name" value="{!! \Auth::check() ?  \Auth::user()->name : '' !!}" />
			@endif
			<input class="input100" type="text" @if(\Auth::check()) disabled @endif  name="name" value="{!! \Auth::check() ?  \Auth::user()->name : '' !!}" placeholder="например, Кажыгумар" required>
		</div>

		<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Обязательное поле">
			<i class="fa fa-phone"></i>
			<span class="label-input100">{!! __('Carrier phone') !!}</span>
			<input class="input100" type="text" name="phone" placeholder="+7 (777) 426 05 76">
		</div>

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-up"></i>
			<span class="label-input100">{!!__('Transport type')!!}</span>
			<div>
				<select class="js-select2" id="transport" name="transport_id" required>
					@foreach (\App\Transport::all() as $transport)
					<option value="{!! $transport->id !!}">{!! $transport->name !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Обязательное поле">
			<i class="fa fa-calendar"></i>
			<span class="label-input100">{!!__('Даты отправления - прибытия')!!}</span>
			<input class="input100" type="text" name="dates" required>
		</div>

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-up"></i>
			<span class="label-input100">{!!__('Город отправления')!!}</span> 
			<div>
				<select class="js-select2" name="city_from_id" placeholder="{!! __('Выберите город доставки') !!}">
					@foreach (\App\City::all() as $city)
					<option value="{!! $city->id !!}">{!! $city->name . ', ' . $city->name_kz . ', ' . $city->region !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-down"></i>
			<span class="label-input100">Город прибытия</span>
			<div>
				<select class="js-select2" name="city_to_id" placeholder="{!! __('Выберите город доставки') !!}">
					@foreach (\App\City::all() as $city)
					<option value="{!! $city->id !!}">{!! $city->name . ', ' . $city->name_kz . ', ' . $city->region !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		<div class="w-full js-show-service">
			{{-- <div class="wrap-contact100-form-radio">
				<span class="label-input100">What type of products do you sell?</span>

				<div class="contact100-form-radio m-t-15">
					<input class="input-radio100" id="radio1" type="radio" name="type-product" value="physical" checked="checked">
					<label class="label-radio100" for="radio1">
						Phycical Products
					</label>
				</div>

				<div class="contact100-form-radio">
					<input class="input-radio100" id="radio2" type="radio" name="type-product" value="digital">
					<label class="label-radio100" for="radio2">
						Digital Products
					</label>
				</div>

				<div class="contact100-form-radio">
					<input class="input-radio100" id="radio3" type="radio" name="type-product" value="service">
					<label class="label-radio100" for="radio3">
						Services Consulting
					</label>
				</div>
			</div> 

			<div class="wrap-contact100-form-range">
				<span class="label-input100">{{__("Budget")}}</span>

				<div class="contact100-form-range-value">
					<span id="value-lower">610</span> тенге - <span id="value-upper">980</span> тенге
					<input type="text" name="price_start">
					<input type="text" name="price_end">
				</div>

				<div class="contact100-form-range-bar">
					<div id="filter-bar"></div>
				</div>
			</div>--}}
		</div>

		{{-- <div class="wrap-input100 validate-input bg0 rs1-alert-validate" data-validate = "Заполните описание">
			<i class="fa fa-envelope-o"></i>
			<span class="label-input100">Описание отправления</span>
			<textarea class="input100" name="message" placeholder="например, коробка с вещами весом 4кг"></textarea>
		</div> --}}

		<div class="container-contact100-form-btn">
			<button class="contact100-form-btn" type="submit">
				<span>
					{!! __('Create carrier') !!}
					<i class="fa fa-save m-l-7" aria-hidden="true"></i> 
				</span>
			</button>
		</div>
	</form>

@endsection

@section('js')
	
<!--===============================================================================================-->
<script src="/vendor/daterangepicker/moment.min.js"></script>
<script src="/vendor/daterangepicker/daterangepicker.js"></script>
<script>
	$('input[name="dates"]').daterangepicker({
		timePicker: false,
		locale: {
			format: 'DD/MM/YYYY'
		}
	});
</script>
<!--===============================================================================================-->
<script src="/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="/vendor/noui/nouislider.min.js"></script>
<script>
	var filterBar = document.getElementById('filter-bar');

	noUiSlider.create(filterBar, {
		start: [ 1500, 3900 ],
		connect: true,
		step: 100,
		range: {
			'min': 0,
			'max': 10000
		}
	});

	var skipValues = [
	document.getElementById('value-lower'),
	document.getElementById('value-upper')
	];

	filterBar.noUiSlider.on('update', function( values, handle ) {
		skipValues[handle].innerHTML = Math.round(values[handle]);
		$('.contact100-form-range-value input[name="price_start"]').val($('#value-lower').html());
		$('.contact100-form-range-value input[name="price_end"]').val($('#value-upper').html());
	});
</script>
<script>
	$(".js-select2").each(function(){
		$(this).select2({
			minimumResultsForSearch: 20,
			dropdownParent: $(this).next('.dropDownSelect2')
		});

	})
</script>
<!--===============================================================================================-->

@endsection