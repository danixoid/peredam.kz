
@extends('app')
@section('content')

	<form class="contact100-form validate-form" action="{!! route('carrier.update',$carrier) !!}" method="POST">
		@csrf
		@method('PUT')

		<span class="contact100-form-title">
			{!! __('Могу передать') !!}
		</span>
		<span class="contact100-form-desc">
			{!! __('Регистрация билета') !!}
		</span>

		<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate="Обязательное поле">
			<i class="fa fa-user-circle"></i>
			<span class="label-input100">Имя курьера</span>
			@if(\Auth::check())
			<input type="hidden" name="name" value="{!! \Auth::check() ?  \Auth::user()->name : '' !!}" />
			@endif
			<input class="input100" type="text" @if(\Auth::check()) disabled @endif  name="name" value="{!! \Auth::check() ?  \Auth::user()->name : '' !!}" placeholder="например, Кажыгумар" required>
		</div>

		<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Обязательное поле">
			<i class="fa fa-phone"></i>
			<span class="label-input100">{!! __('Carrier phone') !!}</span>
			<input class="input100" type="text" name="phone" value="{!! $carrier->phone !!}" placeholder="+7 (777) 426 05 76">
		</div>

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-train"></i>
			<span class="label-input100">{!!__('Transport type')!!}</span>
			<div>
				<select class="js-select2" id="transport" name="transport_id" required>
					@foreach (\App\Transport::all() as $transport)
					<option value="{!! $transport->id !!}">{!! $transport->name !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Обязательное поле">
			<i class="fa fa-calendar"></i>
			<span class="label-input100">Даты отправления - прибытия</span>
			<input class="input100" type="text" name="dates" required>
		</div>

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-up"></i>
			<span class="label-input100">{!!__('Город отправления')!!}</span> 
			<div>
				<select class="js-select2" name="city_from_id" placeholder="{!! __('Выберите город доставки') !!}">
					@foreach (\App\City::all() as $city)
					<option @if($city->id == $carrier->city_to_id) selected @endif
						value="{!! $city->id !!}">{!! $city->name . ', ' . $city->name_kz . ', ' . $city->region !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		<!-- <div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Обязательное поле">
			<i class="fa fa-address-card"></i>
			<span class="label-input100">Введите адрес отправления</span>
			<input class="input100" type="text" name="from_address" placeholder="например, мкр.Самал-2, 87, кв. 55">
		</div> -->

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-down"></i>
			<span class="label-input100">Город прибытия</span>
			<div>
				<select class="js-select2" name="city_to_id" placeholder="{!! __('Выберите город доставки') !!}">
					@foreach (\App\City::all() as $city)
					<option @if($city->id == $carrier->city_from_id) selected @endif
						value="{!! $city->id !!}">{!! $city->name . ', ' . $city->name_kz . ', ' . $city->region !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		{{-- <div class="w-full dis-none js-show-service">
			<div class="wrap-contact100-form-radio">
				<span class="label-input100">What type of products do you sell?</span>

				<div class="contact100-form-radio m-t-15">
					<input class="input-radio100" id="radio1" type="radio" name="type-product" value="physical" checked="checked">
					<label class="label-radio100" for="radio1">
						Phycical Products
					</label>
				</div>

				<div class="contact100-form-radio">
					<input class="input-radio100" id="radio2" type="radio" name="type-product" value="digital">
					<label class="label-radio100" for="radio2">
						Digital Products
					</label>
				</div>

				<div class="contact100-form-radio">
					<input class="input-radio100" id="radio3" type="radio" name="type-product" value="service">
					<label class="label-radio100" for="radio3">
						Services Consulting
					</label>
				</div>
			</div>

			<div class="wrap-contact100-form-range">
				<span class="label-input100">Budget</span>

				<div class="contact100-form-range-value">
					$<span id="value-lower">610</span> - $<span id="value-upper">980</span>
					<input type="text" name="from-value">
					<input type="text" name="to-value">
				</div>

				<div class="contact100-form-range-bar">
					<div id="filter-bar"></div>
				</div>
			</div>
		</div> --}}

		{{-- <div class="wrap-input100 validate-input bg0 rs1-alert-validate" data-validate = "Заполните описание">
			<i class="fa fa-envelope-o"></i>
			<span class="label-input100">{!! __('Описание') !!}</span>
			<textarea class="input100" name="message" placeholder="например, есть три места">{{ $carrier->notes }}</textarea>
		</div> --}}


		<div class="rs1-wrap-input100">
			<a class="contact100-form-btn" href="{!! route('carrier.show',$carrier->id) !!}">
				<span>
					{!! __('Go Back') !!}
					<i class="fa fa-arrow-left m-l-7" aria-hidden="true"></i>
				</span>
			</a>
		</div>

		<div class="rs1-wrap-input100">
			<button class="contact100-form-btn">
				<span>
					<i class="fa fa-save m-r-7" aria-hidden="true"></i>
					{!! __('Save carrier') !!}
				</span>
			</button>
		</div>
	</form>

@endsection

@section('js')
	
<!--===============================================================================================-->
	<script src="vendor/select2/select2.min.js"></script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});


			// $(".js-select2").each(function(){
			// 	$(this).on('select2:close', function (e){
			// 		if($(this).val() == "Please chooses") {
			// 			$('.js-show-service').slideUp();
			// 		}
			// 		else {
			// 			$('.js-show-service').slideUp();
			// 			$('.js-show-service').slideDown();
			// 		}
			// 	});
			// });
		})
	</script>
<!--===============================================================================================-->
<script src="/vendor/daterangepicker/moment.min.js"></script>
<script src="/vendor/daterangepicker/daterangepicker.js"></script>
	<script>
		$('input[name="dates"]').daterangepicker({
			timePicker: false,
			startDate: moment('{!! $carrier->departure_at !!}','YYYY-MM-DD'),
			endDate: moment('{!! $carrier->arrival_at !!}','YYYY-MM-DD'),
			locale: {
				format: 'DD/MM/YYYY'
			}
		});
	</script>
<!--===============================================================================================-->
	<script src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="vendor/noui/nouislider.min.js"></script>
	<script>
	    var filterBar = document.getElementById('filter-bar');

	    noUiSlider.create(filterBar, {
	        start: [ 1500, 3900 ],
	        connect: true,
	        range: {
	            'min': 1500,
	            'max': 7500
	        }
	    });

	    var skipValues = [
	    document.getElementById('value-lower'),
	    document.getElementById('value-upper')
	    ];

	    filterBar.noUiSlider.on('update', function( values, handle ) {
	        skipValues[handle].innerHTML = Math.round(values[handle]);
	        $('.contact100-form-range-value input[name="from-value"]').val($('#value-lower').html());
	        $('.contact100-form-range-value input[name="to-value"]').val($('#value-upper').html());
	    });
	</script>
<!--===============================================================================================-->

@endsection