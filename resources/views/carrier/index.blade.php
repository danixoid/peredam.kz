
@extends('app')
@section('content')

	<form class="contact100-form validate-form" action="">
		<span class="contact100-form-title">
			Могу передать
		</span>
		<span class="contact100-form-desc">
			Регистрация билета
		</span>

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-train"></i>
			<span class="label-input100">Способ доставки</span>
			<div>
				<select class="js-select2" id="transport" name="transport_id">
					<option value="">{!! __('Выберите значение') !!}</option>
					@foreach (\App\Transport::all() as $transport)
					<option value="{!! $transport->id !!}"
						@if(request('transport_id') == $transport->id) selected @endif
					>{!! $transport->name !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>
		
		<div class="wrap-input100 validate-input bg1 rs1-wrap-input100" data-validate = "Обязательное поле">
			<i class="fa fa-calendar"></i>
			<span class="label-input100">Даты отправления - прибытия</span>
			<input class="input100" type="text" name="dates" required>
		</div>


		
		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-up"></i>
			<span class="label-input100">Город отправления</span> 
			<div>
				<select class="js-select2" name="city_from_id" placeholder="{!! __('Выберите город отправления') !!}">
					<option value="">{!! __('Выберите значение') !!}</option>
					@foreach (\App\City::all() as $city)
					<option
						@if($city->id == request('city_from_id')) selected @endif
						value="{!! $city->id !!}">{!! $city->name . ', ' . $city->name_kz . ', ' . $city->region !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
			<i class="fa fa-arrow-down"></i>
			<span class="label-input100">Город доставки</span>
			<div>
				<select class="js-select2" name="city_to_id" placeholder="{!! __('Выберите город доставки') !!}">
					<option value="">{!! __('Выберите значение') !!}</option>
					@foreach (\App\City::all() as $city)
					<option 
						@if($city->id == request('city_to_id')) selected @endif
						value="{!! $city->id !!}">{!! $city->name . ', ' . $city->name_kz . ', ' . $city->region !!}</option>
					@endforeach
				</select>
				<div class="dropDownSelect2"></div>
			</div>
		</div>

		{{-- <div class="w-full js-show-service">
			
			<div class="wrap-contact100-form-range">
				<span class="label-input100">{{__("Budget")}}</span>

				<div class="contact100-form-range-value">
					<span id="value-lower">610</span> тенге - <span id="value-upper">980</span> тенге
					<input type="text" name="from-value">
					<input type="text" name="to-value">
				</div>

				<div class="contact100-form-range-bar">
					<div id="filter-bar"></div>
				</div>
			</div>
		</div> --}}

		<div class="rs1-wrap-input100">
			<button class="contact100-form-btn">
				<span>
					{!! __('Search') !!}
					<i class="fa fa-search m-l-7" aria-hidden="true"></i>
				</span>
			</button>
		</div>
		
		<div class="contact100-form mt-2">
			@foreach ($carriers as $carrier)
			<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
				{{-- <img class="card-img-top" src="/" alt="Card image cap"> --}}
				<div class="card-body">
					<h5 class="card-title">{!! $carrier->name !!}</h5>
					<p class="card-text"><strong><pre>{!! $carrier->notes !!}</pre></strong></p>
					<p class="card-text"><strong>{!! __('Город отправления/прибытия') !!}</strong>: {!! $carrier->city_from->name !!} / {!! $carrier->city_to->name !!}</p>
					<p class="card-text"><strong>{!! __('Время отправления / прибытия') !!}</strong>: {!! date('d/m/Yг',strtotime($carrier->departure_at)) !!} - {!! date('d/m/Yг',strtotime($carrier->arrival_at)) !!}</p>
					@if(\Auth::check())
					<a href="{!! route('carrier.show',$carrier->id) !!}" class="btn btn-link">{{__('Show')}}</a>
					@else
					<a href="{!! route('login') !!}" class="btn btn-link">{!! __('Чтобы увидеть детали, войдите в систему') !!}</a>
					@endif
				</div>
			</div>	
			@endforeach
		</div>
		<div class="contact100-form mt-2">
			<div class="rs1-wrap-input100 mt-2">
				<a href="{!! route('carrier.create') !!}" class="contact100-form-btn">
					<span>
						{!! __('Зарегистрировать рейс')!!}
						<i class="fa fa-save m-l-7" aria-hidden="true"></i>
					</span>
				</a>
			</div>
		</div>
	</form>

@endsection

@section('js')
	
<!--===============================================================================================-->
<script src="/vendor/daterangepicker/moment.min.js"></script>
<script src="/vendor/daterangepicker/daterangepicker.js"></script>
<script>
	$('input[name="dates"]').daterangepicker({
		timePicker: false,
		@if(request()->has('dates'))
		startDate: moment('{!! explode(' - ',request('dates'))[0] !!}','DD/MM/YYYY'),
		endDate: moment('{!! explode(' - ',request('dates'))[1] !!}','DD/MM/YYYY'),
		@endif
		locale: {
			format: 'DD/MM/YYYY'
		}
	});
</script>
<!--===============================================================================================-->
<script src="/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script src="/vendor/noui/nouislider.min.js"></script>
<script>
	var filterBar = document.getElementById('filter-bar');

	noUiSlider.create(filterBar, {
		start: [ 1500, 3900 ],
		connect: true,
		step: 100,
		range: {
			'min': 0,
			'max': 10000
		}
	});

	var skipValues = [
	document.getElementById('value-lower'),
	document.getElementById('value-upper')
	];

	filterBar.noUiSlider.on('update', function( values, handle ) {
		skipValues[handle].innerHTML = Math.round(values[handle]);
		$('.contact100-form-range-value input[name="from-value"]').val($('#value-lower').html());
		$('.contact100-form-range-value input[name="to-value"]').val($('#value-upper').html());
	});
</script>
<script>
	$(".js-select2").each(function(){
		$(this).select2({
			minimumResultsForSearch: 20,
			dropdownParent: $(this).next('.dropDownSelect2')
		});


		// $(".js-select2").each(function(){
		// 	$(this).on('select2:close', function (e){
		// 		if($(this).val() == "Please chooses") {
		// 			$('.js-show-service').slideUp();
		// 		}
		// 		else {
		// 			$('.js-show-service').slideUp();
		// 			$('.js-show-service').slideDown();
		// 		}
		// 	});
		// });
	})
</script>
<!--===============================================================================================-->

@endsection