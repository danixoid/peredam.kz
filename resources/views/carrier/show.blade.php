

@extends('app')

@section('content')

		<form class="contact100-form" action="{!! route('carrier.index') !!}" method="GET">
			{{-- @csrf --}}
			<span class="contact100-form-title">
				{!! __('Могу отправить') !!}
			</span>
			<span class="contact100-form-desc">
				{!! $carrier->notes !!}
			</span>

			<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
				<i class="fa fa-building-o"></i>
				<span class="label-input100">{{ __('Город отправления') }}</span> 
				<p class="label-input100">{!! $carrier->city_from->full_name!!}</p> 
				
			</div>
			
			<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
				<i class="fa fa-building-o"></i>
				<span class="label-input100">{{ __('Город доставки') }}</span>
				<p class="label-input100">{!! $carrier->city_to->full_name!!}</p> 
			</div>

			<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
				<i class="fa fa-clock-o"></i>
				<span class="label-input100">{{ __('Date From/To') }}</span> 
				<p class="label-input100">{!! date('d/m/Yг',strtotime($carrier->departure_at)) !!} - {!! date('d/m/Yг',strtotime($carrier->arrival_at)) !!}</p> 
				
			</div>

			
			<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
				<i class="fa fa-train"></i>
				<span class="label-input100">{!!__('Transport type')!!}</span>
				<p class="label-input100">{!! $carrier->transport->name !!}</p> 
			</div>

			<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
				<i class="fa fa-user"></i>
				<span class="label-input100">{{ __('Carrier name') }}</span> 
				<p class="label-input100">{!! $carrier->name!!}</p> 
				
			</div>

			<div class="wrap-input100 input100-select bg1 rs1-wrap-input100">
				<a href="tel:{!! preg_replace('/[^\d\+]/i','',$carrier->phone) !!}">
					<i class="fa fa-phone"></i>
					<span class="label-input100">{{ __('Carrier phone') }}</span> 
					<p class="label-input100">{!! $carrier->phone!!}</p>
				</a>
				
			</div>

		</form>

		@if($carrier->user_id == \Auth::user()->id)

		<form class="contact100-form" action="{!! route('carrier.destroy',$carrier) !!}" method="POST">
			@csrf
			@method('DELETE')

			<div class="rs1-wrap-input100">
				<a class="contact100-form-btn" href="{!! route('carrier.edit',$carrier->id) !!}">
					<span>
						{!! __('Edit') !!}
						<i class="fa fa-edit m-l-7" aria-hidden="true"></i>
					</span>
				</a>
			</div>
			
			<div class="rs1-wrap-input100">
				<button class="contact100-form-btn" type="submit" onclick="return confirm('Удалить?')">
					<span>
						{!! __('Soft Delete') !!}
						<i class="fa fa-trash m-l-7" aria-hidden="true"></i>
					</span>
				</button>
			</div>
		</form>
	@endif



@endsection


@section('js')
<!--===============================================================================================-->
	<script src="/vendor/daterangepicker/moment.min.js"></script>
	<script src="/vendor/daterangepicker/daterangepicker.js"></script>
	<script>
		$('input[name="dates"]').daterangepicker();
	</script>
<!--===============================================================================================-->
	<script src="/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="/vendor/noui/nouislider.min.js"></script>
	<script>
	    var filterBar = document.getElementById('filter-bar');

	    noUiSlider.create(filterBar, {
	        start: [ 1500, 3900 ],
	        connect: true,
	        range: {
	            'min': 1500,
	            'max': 7500
	        }
	    });

	    var skipValues = [
	    document.getElementById('value-lower'),
	    document.getElementById('value-upper')
	    ];

	    filterBar.noUiSlider.on('update', function( values, handle ) {
	        skipValues[handle].innerHTML = Math.round(values[handle]);
	        $('.contact100-form-range-value input[name="from-value"]').val($('#value-lower').html());
	        $('.contact100-form-range-value input[name="to-value"]').val($('#value-upper').html());
	    });
	</script>
	<script>
		$(".js-select2").each(function(){
			$(this).select2({
				minimumResultsForSearch: 20,
				dropdownParent: $(this).next('.dropDownSelect2')
			});
		})
	</script>
@endsection