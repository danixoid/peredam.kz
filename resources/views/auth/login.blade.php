

@extends('app')

@section('content')
    <span class="contact100-form-title"  method="POST" action="{{ route('login') }}">
        {{ __('Choose auth method') }}
    </span>
    {{-- <span class="contact100-form-desc">
        {{ __('Choose auth method') }}
    </span> --}}

    <div class="wrap-input100 wrap-social">
        {{-- <a href="{{ url('/auth/redirect/twitter') }}" class="social social-vk"><i class="fa fa-twitter"></i></a> --}}
        <a href="{{ url('/auth/redirect/facebook') }}" class="social social-facebook"><i class="fa fa-facebook"></i></a>
        <a href="{{ url('/auth/redirect/google') }}" class="social social-google"><i class="fa fa-google"></i></a>
    </div>


    <span class="contact100-form-title" >
        {{ __('Or By Email') }}
    </span>

    <form class=" validate-form" method="POST" action="{{ route('login') }}">
        
        @csrf

        <div class="form-group row">

            <div class="col-md-5">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" 
                    placeholder="{{ __('E-Mail Address') }}"
                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="col-md-5">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                placeholder="{{ __('Password') }}"
                name="password" required autocomplete="current-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
            {{-- <div class="col-md-6">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                    <label class="form-check-label" for="remember">
                        {{ __('Remember Me') }}
                    </label>
                </div>
            </div> --}}
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary btn-block">
                    {{ __('Login') }}
                </button>

            </div>
        </div>
    </form>
    @if (Route::has('password.request'))
        <a class="btn btn-link" href="{{ route('password.request') }}">
            {{ __('Forgot Your Password?') }}
        </a>
        <a class="btn btn-link" href="{{ route('register') }}">
            {{ __('Registration') }}
        </a>
    @endif

@endsection
@section('js')
<script>
</script>
@endsection
