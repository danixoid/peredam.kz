<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'facebook' => [
        'client_id' => '291055072206466',
        'client_secret' => 'baf1e9a07914b11a15194003ce36fc39',
        'redirect' => 'https://peredam.kz/callback/facebook',
    ],
    
    'google' => [
        'client_id'     => '199033359051-3q2audo8ptdij53ia1ngn5ft47s5l8f4.apps.googleusercontent.com',
        'client_secret' => 'fyp-D8TCJi6gkB15PNcqb9-S',
        'redirect'      => 'https://peredam.kz/callback/google',
    ],
];
