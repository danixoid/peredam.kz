<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CitySeeder::class);

        DB::table('users')->insert([
            'name' => 'Daniyar Saumbayev',
            'email' => 'danixoid@gmail.com',
            'password' => Hash::make('Roamer20')
        ]);

        // // FAKE USERS -- REMOVE
        // for($i=0;$i<10;$i++) {
        //     DB::table('users')->insert([
        //         'name' => Str::random(10),
        //         'email' => Str::random(10).'@gmail.com',
        //         'password' => Hash::make('password'),
        //     ]);
        // }

        //  
        DB::table('transports')->insert(['name' => 'Авиа']);
        DB::table('transports')->insert(['name' => 'Железная дорога']);
        DB::table('transports')->insert(['name' => 'Автобус']);
        
    }
}
