<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('senders', function (Blueprint $table) {
            $table->id();
            $table->string('name_from');
            $table->string('phone_from');
            $table->dateTime('date_from');
            $table->unsignedBigInteger('city_from_id');
            $table->string('address_from')->nullable();
            $table->string('name_to')->nullable();
            $table->string('phone_to')->nullable();
            $table->dateTime('date_to');
            $table->unsignedBigInteger('city_to_id');
            $table->string('address_to')->nullable();
            $table->integer('price')->nullable();
            $table->text('notes')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('city_from_id')->references('id')->on('cities');
            $table->foreign('city_to_id')->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('senders');
    }
}
