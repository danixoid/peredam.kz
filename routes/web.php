<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/carrier', 'HomeController@carrier')->name('carrier');
Route::get('/policy', 'HomeController@policy')->name('policy');
Route::get('/agreements', 'HomeController@agreements')->name('agreements');
Route::get('/auth/redirect/{provider}', 'SocialController@redirect');
Route::get('/callback/{provider}', 'SocialController@callback');

Route::resources([
    'sender' => 'SenderController',
    'carrier' => 'CarrierController'
]);

Auth::routes();
